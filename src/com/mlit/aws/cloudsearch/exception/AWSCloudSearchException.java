/**
 * 
 */
package com.mlit.aws.cloudsearch.exception;

/**
 * @author Marcos Loiola - marcos.loiola@gmail.com
 *
 */
public class AWSCloudSearchException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4692722561479921536L;

	public AWSCloudSearchException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AWSCloudSearchException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}
