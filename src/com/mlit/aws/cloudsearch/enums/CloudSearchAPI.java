/**
 * 
 */
package com.mlit.aws.cloudsearch.enums;

/**
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public enum CloudSearchAPI {

	API_2013("2013-01-01");

	private String apiURL;

	private CloudSearchAPI(String apiURL) {
		this.apiURL = apiURL;
	}

	public String getApiURL() {
		return apiURL;
	}

}
