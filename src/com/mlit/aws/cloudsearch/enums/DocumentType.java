/**
 * 
 */
package com.mlit.aws.cloudsearch.enums;

/**
 * @author Marcos Loiola - marcos.loiola@gmail.com
 *
 */
public enum DocumentType {

	add, delete

}
