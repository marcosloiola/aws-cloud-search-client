package com.mlit.aws.cloudsearch.test;

import java.util.List;

public class MovieFields {
	private List<String> actors;
	private List<String> directors;
	private List<String> genres;
	private String image_url;
	private String plot;
	private String rank;
	private String rating;
	private String release_date;
	private String running_time_secs;
	private String title;
	private String year;

	public List<String> getActors() {
		return actors;
	}

	public void setActors(List<String> actors) {
		this.actors = actors;
	}

	public List<String> getDirectors() {
		return directors;
	}

	public void setDirectors(List<String> directors) {
		this.directors = directors;
	}

	public List<String> getGenres() {
		return genres;
	}

	public void setGenres(List<String> genres) {
		this.genres = genres;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getPlot() {
		return plot;
	}

	public void setPlot(String plot) {
		this.plot = plot;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getRelease_date() {
		return release_date;
	}

	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}

	public String getRunning_time_secs() {
		return running_time_secs;
	}

	public void setRunning_time_secs(String running_time_secs) {
		this.running_time_secs = running_time_secs;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "MovieFields [actors=" + actors + ", directors=" + directors + ", genres=" + genres + ", image_url=" + image_url + ", plot=" + plot + ", rank=" + rank + ", rating=" + rating + ", release_date=" + release_date + ", running_time_secs=" + running_time_secs + ", title=" + title + ", year=" + year + "]";
	}
	
	

}
