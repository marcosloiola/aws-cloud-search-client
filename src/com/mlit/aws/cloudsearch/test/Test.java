/**
 * 
 */
package com.mlit.aws.cloudsearch.test;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mlit.aws.cloudsearch.AWSCloudSearchClient;
import com.mlit.aws.cloudsearch.bean.search.request.CSSearchRequest;
import com.mlit.aws.cloudsearch.bean.search.request.Cursor;
import com.mlit.aws.cloudsearch.bean.search.request.Expr;
import com.mlit.aws.cloudsearch.bean.search.request.Format;
import com.mlit.aws.cloudsearch.bean.search.request.Format.ContentType;
import com.mlit.aws.cloudsearch.bean.search.request.Fq;
import com.mlit.aws.cloudsearch.bean.search.request.Highlight;
import com.mlit.aws.cloudsearch.bean.search.request.Highlight.HighlightFormat;
import com.mlit.aws.cloudsearch.bean.search.request.Partial;
import com.mlit.aws.cloudsearch.bean.search.request.Pretty;
import com.mlit.aws.cloudsearch.bean.search.request.Q;
import com.mlit.aws.cloudsearch.bean.search.request.QOption;
import com.mlit.aws.cloudsearch.bean.search.request.QParser;
import com.mlit.aws.cloudsearch.bean.search.request.QParser.Parser;
import com.mlit.aws.cloudsearch.bean.search.request.Return;
import com.mlit.aws.cloudsearch.bean.search.request.Size;
import com.mlit.aws.cloudsearch.bean.search.request.Sort;
import com.mlit.aws.cloudsearch.bean.search.request.Start;
import com.mlit.aws.cloudsearch.bean.search.response.CSSearchResponse;
import com.mlit.aws.cloudsearch.enums.CloudSearchAPI;

/**
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class Test {

	private static final boolean USE_CURSOR = true;
	private static final boolean USE_EXPR = false;
	private static final boolean USE_FACET = false;
	private static final boolean USE_FORMAT = false;
	private static final boolean USE_FQ = false;
	private static final boolean USE_HIGHLIGHT = true;
	private static final boolean USE_PARTIAL = false;
	private static final boolean USE_PRETTY = false;
	private static final boolean USE_Q = true;
	private static final boolean USE_QOPTION = false;
	private static final boolean USE_QPARSER = false;
	private static final boolean USE_RETURN = false;
	private static final boolean USE_SIZE = false;
	private static final boolean USE_SORT = false;
	private static final boolean USE_START = false;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {

			CSSearchRequest request = new CSSearchRequest();

			/*
			 * Cursor example
			 */

			if (USE_CURSOR) {
				Cursor cursor = new Cursor("");
				request.setCursor(cursor);
			}

			/*
			 * Expr example
			 */
			if (USE_EXPR) {
				Expr expr = new Expr("key1", "value1");
				expr.addExpression("key2", "value2");
				request.setExpr(expr);
			}

			/*
			 * Facet with size and sort example
			 */
			if (USE_FACET) {
				// Facet facet = new Facet("year");
				// facet.setSize(2);
				// facet.setSort(Facet.FacetSort.COUNT);
				// request.setFacet(facet);

				/*
				 * Facet with bucket range values example
				 */
				// List<String> buckets = new ArrayList<String>();
				// buckets.add("[2013,2014]");
				// Facet facet = new Facet("year");
				// facet.setBuckets(buckets);
				// request.setFacet(facet);

				/*
				 * Facet with bucket individual values example
				 */
				// List<String> buckets = new ArrayList<String>();
				// buckets.add("Action");
				// buckets.add("Adventure");
				// buckets.add("Sci-Fi");
				//
				// Facet facet = new Facet("field");
				// facet.setBuckets(buckets);
				// request.setFacet(facet);
			}

			/*
			 * Format example
			 */
			if (USE_FORMAT) {
				Format format = new Format(ContentType.JSON);
				request.setFormat(format);
			}

			/*
			 * Fq example
			 */

			if (USE_FQ) {
				Fq fq = new Fq();
				fq.setValue("fq_value");
				request.setFq(fq);
			}

			/*
			 * Highlight example
			 */
			if (USE_HIGHLIGHT) {
				Highlight highlight = new Highlight();
				Highlight.HighlightObject highlightObject = highlight.new HighlightObject("title");
				highlightObject.setFormat(HighlightFormat.TEXT);
				highlightObject.setMaxPhrases(8);
				// highlightObject.setPreTag("<b><i>");
				// highlightObject.setPostTag("</i></b>");
				highlight.addHighlightObject(highlightObject);

				highlightObject = highlight.new HighlightObject("year");
				highlightObject.setFormat(HighlightFormat.TEXT);
				highlightObject.setMaxPhrases(8);
				// highlightObject.setPreTag("<b><i>");
				// highlightObject.setPostTag("</i></b>");
				highlight.addHighlightObject(highlightObject);

				highlightObject = highlight.new HighlightObject("actors");
				highlightObject.setFormat(HighlightFormat.TEXT);
				highlightObject.setMaxPhrases(8);
				// highlightObject.setPreTag("<b><i>");
				// highlightObject.setPostTag("</i></b>");
				highlight.addHighlightObject(highlightObject);

				request.setHighlight(highlight);

			}

			/*
			 * Partial Example
			 */
			if (USE_PARTIAL) {
				Partial partial = new Partial(false);
				request.setPartial(partial);
			}

			/*
			 * Pretty Example
			 */
			if (USE_PRETTY) {
				Pretty pretty = new Pretty(true);
				request.setPretty(pretty);
			}

			/*
			 * Q example
			 */
			if (USE_Q) {
				Q q = new Q("wolverine");
				request.setQ(q);
			}

			/*
			 * Q Option Example
			 */
			if (USE_QOPTION) {
				QOption qOption = new QOption();
				qOption.setDefaultOperator("OR");
				qOption.setExplicitPhraseSlop(5);
				qOption.setPhraseSlop(3);
				qOption.setTieBreaker(0.2);

				List<String> fields = new ArrayList<String>();
				fields.add("field1");
				fields.add("field2");
				fields.add("field3");
				qOption.setFields(fields);

				List<String> operators = new ArrayList<String>();
				operators.add("operator1");
				operators.add("operator2");
				operators.add("operator3");
				qOption.setOperators(operators);

				List<String> phraseFields = new ArrayList<String>();
				phraseFields.add("phraseFields1");
				phraseFields.add("phraseFields2");
				phraseFields.add("phraseFields3");
				qOption.setPhraseFields(phraseFields);
				request.setqOption(qOption);
			}

			/*
			 * Q Parser example
			 */
			if (USE_QPARSER) {
				QParser qParser = new QParser(Parser.SIMPLE);
				// QParser qParser = new QParser(Parser.DISMAX);
				// QParser qParser = new QParser(Parser.LUCENE);
				// QParser qParser = new QParser(Parser.STRUCTURED);
				request.setqParser(qParser);
			}

			/*
			 * Return example
			 */
			if (USE_RETURN) {
				Return returnFields = new Return();
				returnFields.addValue("title");
				returnFields.addValue("actors");
				returnFields.addValue("year");
				request.setReturnFields(returnFields);
			}

			/*
			 * Size example
			 */
			if (USE_SIZE) {
				Size size = new Size(20);
				request.setSize(size);
			}

			/*
			 * Sort example
			 */
			if (USE_SORT) {
				Sort sort = new Sort();
				sort.addValue("sort1");
				sort.addValue("sort2");
				request.setSort(sort);
			}

			/*
			 * Start example
			 */
			if (USE_START) {
				Start start = new Start(15);
				request.setStart(start);
			}

			AWSCloudSearchClient cloudSearch = AWSCloudSearchClient.getInstance("search-movies-y6gelr4lv3jeu4rvoelunxsl2e.us-east-1.cloudsearch.amazonaws.com", CloudSearchAPI.API_2013);
			System.out.println(request);
			CSSearchResponse response = cloudSearch.search(request);
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			System.out.println(gson.toJson(response));

//			MovieFields fields = new Gson().fromJson(new Gson().toJson(response.getHits().getHit().get(0).getFields()), MovieFields.class);
//			System.out.println(fields);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
