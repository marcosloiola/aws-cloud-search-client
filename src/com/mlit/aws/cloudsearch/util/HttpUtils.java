/**
 * 
 */
package com.mlit.aws.cloudsearch.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.Gson;
import com.mlit.aws.cloudsearch.bean.document.response.CSDocumentBatchResponse;
import com.mlit.aws.cloudsearch.bean.search.response.CSSearchResponse;
import com.mlit.aws.cloudsearch.exception.AWSCloudSearchException;

/**
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class HttpUtils {

	public CSSearchResponse request(String requestURL) {

		InputStream is = null;

		URL url = null;
		HttpURLConnection httpConn = null;
		BufferedReader r = null;

		try {

			requestURL = requestURL.replace(" ", "%20");
			url = new URL(requestURL);
			httpConn = (HttpURLConnection) url.openConnection();
			httpConn.connect();

			is = httpConn.getInputStream();

			if (httpConn.getResponseCode() != 200) {
				throw new AWSCloudSearchException("Invalid response code recieved:  " + httpConn.getResponseCode() + " (" + httpConn.getResponseMessage() + ")");
			}

			r = new BufferedReader(new InputStreamReader(is, "UTF-8"));

			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = r.readLine()) != null) {
				sb.append(line);
			}

			CSSearchResponse awsCloudSearchResponse = new Gson().fromJson(sb.toString(), CSSearchResponse.class);

			return awsCloudSearchResponse;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (IOException e) { /* Ignore */
				}
			if (r != null)
				try {
					r.close();
				} catch (IOException e) { /* Ignore */
				}
			if (httpConn != null)
				httpConn.disconnect();
		}

		return null;
	}

	@SuppressWarnings("resource")
	public CSDocumentBatchResponse postJsonFile(String requestURL, File file) throws IOException {

		URL url = new URL(requestURL);
		HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
		httpConn.setRequestMethod("POST");
		httpConn.setDoOutput(true);
		httpConn.setDoInput(true);
		httpConn.addRequestProperty("Content-Type", "application/json");
		httpConn.addRequestProperty("Content-Length", String.valueOf(file.length()));
		httpConn.connect();

		OutputStream os = httpConn.getOutputStream();

		BufferedReader br = new BufferedReader(new FileReader(file));

		String line;
		while ((line = br.readLine()) != null) {
			os.write(line.getBytes());
		}

		os.flush();
		os.close();

		InputStream is = httpConn.getInputStream();
		br = new BufferedReader(new InputStreamReader(is, "UTF-8"));

		StringBuilder sb = new StringBuilder();
		while ((line = br.readLine()) != null) {
			sb.append(line);
		}

		String response = sb.toString();

		CSDocumentBatchResponse csDocumentBatchResponse = new Gson().fromJson(response, CSDocumentBatchResponse.class);
		return csDocumentBatchResponse;

	}

}
