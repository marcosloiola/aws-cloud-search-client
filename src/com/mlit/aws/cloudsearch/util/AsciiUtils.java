package com.mlit.aws.cloudsearch.util;

import org.apache.commons.lang3.StringUtils;

public class AsciiUtils {

	public static String convertNonAscii(String s) {
		return StringUtils.stripAccents(s);
	}

}
