package com.mlit.aws.cloudsearch.bean.document.response;

public class CSDocumentBatchResponse {

	// {"status": "success", "adds": 115, "deletes": 0}

	private String status;
	private Integer adds;
	private Integer deletes;

	public CSDocumentBatchResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getAdds() {
		return adds;
	}

	public void setAdds(Integer adds) {
		this.adds = adds;
	}

	public Integer getDeletes() {
		return deletes;
	}

	public void setDeletes(Integer deletes) {
		this.deletes = deletes;
	}

	@Override
	public String toString() {
		return "CSDocumentBatchResponse [status=" + status + ", adds=" + adds + ", deletes=" + deletes + "]";
	}

}
