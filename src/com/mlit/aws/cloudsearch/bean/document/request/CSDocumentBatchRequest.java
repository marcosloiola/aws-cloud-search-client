/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.document.request;

import java.io.File;

/**
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class CSDocumentBatchRequest {

	private File file;

	public CSDocumentBatchRequest(File file) {
		super();
		this.file = file;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	@Override
	public String toString() {
		return "CSDocumentBatchRequest [file=" + file + "]";
	}

}
