/**
 * 
 */
package com.mlit.aws.cloudsearch.bean;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.mlit.aws.cloudsearch.enums.DocumentType;

/**
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class CloudSearchDocument {

	private DocumentType type;

	private String id;

	private Map<String, Object> fields;

	public CloudSearchDocument() {
		super();
		fields = new HashMap<String, Object>();
	}

	public CloudSearchDocument(DocumentType type, String id, Integer version, String lang, Map<String, Object> fields) {
		super();
		this.type = type;
		this.id = id;
		this.fields = fields;
	}

	public DocumentType getType() {
		return type;
	}

	public void setType(DocumentType type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, Object> getFields() {
		return fields;
	}

	public void setFields(Map<String, Object> fields) {
		this.fields = fields;
	}

	@Override
	public String toString() {
		return "CloudSearchDocument [type=" + type + ", id=" + id + ", fields=" + fields + "]";
	}

	public String toCloudSearchDocument() {
		return new Gson().toJson(this);
	}

}
