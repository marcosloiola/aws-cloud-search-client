
package com.mlit.aws.cloudsearch.bean.search.response;

public class CSSearchResponse{
   	private Hits hits;
   	private Status status;

 	public Hits getHits(){
		return this.hits;
	}
	public void setHits(Hits hits){
		this.hits = hits;
	}
 	public Status getStatus(){
		return this.status;
	}
	public void setStatus(Status status){
		this.status = status;
	}
}
