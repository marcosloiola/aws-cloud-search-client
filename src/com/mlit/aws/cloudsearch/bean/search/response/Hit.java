package com.mlit.aws.cloudsearch.bean.search.response;

import java.util.Map;

public class Hit {
	private Map<String, Object> fields;
	private String id;

	public Map<String, Object> getFields() {
		return fields;
	}

	public void setFields(Map<String, Object> fields) {
		this.fields = fields;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
