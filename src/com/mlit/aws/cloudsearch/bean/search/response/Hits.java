package com.mlit.aws.cloudsearch.bean.search.response;

import java.util.List;

public class Hits {
	private Number found;
	private List<Hit> hit;
	private Number start;
	private String cursor;

	public Number getFound() {
		return this.found;
	}

	public void setFound(Number found) {
		this.found = found;
	}

	public List<Hit> getHit() {
		return this.hit;
	}

	public void setHit(List<Hit> hit) {
		this.hit = hit;
	}

	public Number getStart() {
		return this.start;
	}

	public void setStart(Number start) {
		this.start = start;
	}

	public String getCursor() {
		return cursor;
	}

	public void setCursor(String cursor) {
		this.cursor = cursor;
	}

}
