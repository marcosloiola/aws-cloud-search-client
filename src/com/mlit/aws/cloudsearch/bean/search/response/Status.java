package com.mlit.aws.cloudsearch.bean.search.response;

import com.google.gson.annotations.SerializedName;

public class Status {
	private String rid;

	@SerializedName("time-ms")
	private Number time;

	public String getRid() {
		return this.rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	public Number getTime() {
		return time;
	}

	public void setTime(Number time) {
		this.time = time;
	}

}
