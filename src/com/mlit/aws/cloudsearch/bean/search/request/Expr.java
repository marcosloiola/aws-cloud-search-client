/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

import java.util.HashMap;
import java.util.Map;

/**
 * Expr
 * 
 * Defines an expression that can be used to sort results or specify search or
 * filter criteria. You can also specify an expression as a return field. You
 * can define and use multiple expressions in a search request.
 * 
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class Expr extends CloudSearchRequestParameter {

	private Map<String, String> map;

	public Expr() {
		this.map = new HashMap<String, String>();
	}

	public Expr(String key, String value) {
		this.map = new HashMap<String, String>();
		this.map.put(key, value);
	}

	public Expr(Map<String, String> map) {
		this.map = map;
	}

	public Map<String, String> getMap() {
		return map;
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}

	public void addExpression(String key, String value) {
		this.map.put(key, value);
	}

	public void removeExpression(String key) {
		this.map.remove(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mlit.aws.cloudsearch.bean.parameters.CloudSearchRequestParameter#
	 * convertToCloudSearch()
	 */
	@Override
	public String convertToCloudSearch() {

		StringBuilder sb = new StringBuilder();

		String value;
		for (String key : map.keySet()) {
			value = map.get(key);
			sb.append("&expr." + key + "=" + value);
		}

		return sb.toString();

	}

	@Override
	public String toString() {
		return "Expr [map=" + map + "]";
	}

}
