/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

/**
 * Fq
 * 
 * Specifies a structured query that filters the results of a search without
 * affecting how the results are scored and sorted. You use fq in conjunction
 * with the q parameter to filter the documents that match the constraints
 * specified in the q parameter. Specifying a filter just controls which
 * matching documents are included in the results, it has no effect on how they
 * are scored and sorted. The fq parameter supports the full structured query
 * syntax. For more information about using filters, see Filtering Matching
 * Documents. For more information about structured queries, see Structured
 * Search Syntax.
 * 
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class Fq extends CloudSearchRequestParameter {

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String convertToCloudSearch() {
		return "&fq=" + value;
	}

	@Override
	public String toString() {
		return "Fq [value=" + value + "]";
	}

}
