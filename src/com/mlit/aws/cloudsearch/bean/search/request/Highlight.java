/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

import java.util.ArrayList;
import java.util.List;

/**
 * Retrieves highlights for matches in the specified text or text-array field.
 * Highlight options are specified as a JSON object. If the JSON object is
 * empty, the returned field text is treated as HTML and the first match is
 * highlighted with emphasis tags: <em>search-term</em>.
 * 
 * You can specify four options in the JSON object:
 * 
 * format: specifies the format of the data in the text field: text or html.
 * When data is returned as HTML, all non-alphanumeric characters are encoded.
 * The default is html.
 * 
 * max_phrases: specifies the maximum number of occurrences of the search
 * term(s) you want to highlight. By default, the first occurrence is
 * highlighted.
 * 
 * pre_tag: specifies the string to prepend to an occurrence of a search term.
 * The default for HTML highlights is
 * <em>. The default for text highlights is *.
 * 
 * post_tag: specifies the string to append to an occurrence of a search term. The default for HTML highlights is </em>
 * . The default for text highlights is *.
 * 
 * Examples: highlight.plot={},
 * highlight.plot={format:'text',max_phrases:2,pre_tag:'<b>',post_tag:'</b>'}
 * 
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class Highlight extends CloudSearchRequestParameter {

	private List<HighlightObject> highlights;

	public Highlight() {
		this.highlights = new ArrayList<Highlight.HighlightObject>();
	}

	public Highlight(HighlightObject highlightObject) {
		this.highlights = new ArrayList<Highlight.HighlightObject>();
		this.highlights.add(highlightObject);
	}

	public Highlight(List<HighlightObject> highlightObjects) {
		this.highlights = highlightObjects;
	}

	public List<HighlightObject> getHighlights() {
		return highlights;
	}

	public void setHighlights(List<HighlightObject> highlights) {
		this.highlights = highlights;
	}

	public void addHighlightObject(HighlightObject highlightObject) {
		this.highlights.add(highlightObject);
	}

	public void removeHighlightObject(HighlightObject highlightObject) {
		this.highlights.remove(highlightObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mlit.aws.cloudsearch.bean.parameters.CloudSearchRequestParameter#
	 * convertToCloudSearch()
	 */
	@Override
	public String convertToCloudSearch() {

		StringBuilder sb = new StringBuilder();

		for (HighlightObject highlightObject : this.highlights) {
			sb.append(highlightObject.convertToCloudSearch());
		}

		return sb.toString();

	}

	public enum HighlightFormat {
		TEXT, HTML
	}

	public class HighlightObject extends CloudSearchRequestParameter {

		private Integer maxPhrases;

		private String field;

		private String preTag;

		private String postTag;

		private HighlightFormat format;

		public HighlightObject(String field) {
			this.field = field;
			this.format = HighlightFormat.HTML;
			this.preTag = "<em>";
			this.preTag = "</em>";
		}

		public HighlightObject(String field, HighlightFormat format) {
			this.field = field;
			this.format = format;

			if (format.equals(HighlightFormat.HTML)) {
				this.preTag = "<em>";
				this.preTag = "</em>";
			} else if (format.equals(HighlightFormat.TEXT)) {
				this.preTag = "*";
				this.preTag = "*";
			}
		}

		public Integer getMaxPhrases() {
			return maxPhrases;
		}

		public void setMaxPhrases(Integer maxPhrases) {
			this.maxPhrases = maxPhrases;
		}

		public String getField() {
			return field;
		}

		public void setField(String field) {
			this.field = field;
		}

		public String getPreTag() {
			return preTag;
		}

		public void setPreTag(String preTag) {
			this.preTag = preTag;
		}

		public String getPostTag() {
			return postTag;
		}

		public void setPostTag(String postTag) {
			this.postTag = postTag;
		}

		public HighlightFormat getFormat() {
			return format;
		}

		public void setFormat(HighlightFormat format) {
			this.format = format;
		}

		@Override
		public String toString() {
			return "Highlight [maxPhrases=" + maxPhrases + ", field=" + field + ", preTag=" + preTag + ", postTag=" + postTag + ", format=" + format + "]";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((field == null) ? 0 : field.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			HighlightObject other = (HighlightObject) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (field == null) {
				if (other.field != null)
					return false;
			} else if (!field.equals(other.field))
				return false;
			return true;
		}

		private Highlight getOuterType() {
			return Highlight.this;
		}

		@Override
		public String convertToCloudSearch() {

			return ""; 
			
//			StringBuilder sb = new StringBuilder();
//			sb.append("&highlight.").append(this.field).append("=").append("{");
//			if (this.maxPhrases != null) {
//				sb.append("max_phrases:").append(this.maxPhrases).append(",");
//			}
//			sb.append("pre_tag:'").append(this.preTag).append("',");
//			sb.append("post_tag:'").append(this.postTag).append("',");
//			sb.append("format:'").append(this.format.name().toLowerCase()).append("'}");
//
//			return sb.toString();
		}

	}

}
