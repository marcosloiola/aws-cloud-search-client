/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

/**
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public abstract class CloudSearchRequestParameter {

	public abstract String convertToCloudSearch();

}
