/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

/**
 * Specifies which query parser to use to process the request: simple,
 * structured, lucene, and dismax. If q.parser is not specified, Amazon
 * CloudSearch uses the simple query parser.
 * 
 * simple: perform simple searches of text and text-array fields. By default,
 * the simple query parser searches all text and text-array fields. You can
 * specify which fields to search by with the q.options parameter. If you prefix
 * a search term with a plus sign (+) documents must contain the term to be
 * considered a match. (This is the default, unless you configure the default
 * operator with the q.options parameter.) You can use the - (NOT), | (OR), and
 * * (wildcard) operators to exclude particular terms, find results that match
 * any of the specified terms, or search for a prefix. To search for a phrase
 * rather than individual terms, enclose the phrase in double quotes. For more
 * information, see Searching Your Data with Amazon CloudSearch.
 * 
 * structured: perform advanced searches by combining multiple expressions to
 * define the search criteria. You can also search within particular fields,
 * search for values and ranges of values, and use advanced options such as term
 * boosting, matchall, and near. For more information, see Constructing Compound
 * Queries.
 * 
 * lucene: search using the Apache Lucene query parser syntax. For more
 * information, see Apache Lucene Query Parser Syntax.
 * 
 * dismax: search using the simplified subset of the Apache Lucene query parser
 * syntax defined by the DisMax query parser. For more information, see DisMax
 * Query Parser Syntax.
 * 
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class QParser extends CloudSearchRequestParameter {

	private Parser parser;

	public QParser() {
		this.parser = Parser.SIMPLE;
	}

	public QParser(Parser parser) {
		super();
		this.parser = parser;
	}

	public Parser getParser() {
		return parser;
	}

	public void setParser(Parser parser) {
		this.parser = parser;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mlit.aws.cloudsearch.bean.parameters.CloudSearchRequestParameter#
	 * convertToCloudSearch()
	 */
	@Override
	public String convertToCloudSearch() {
		return "&q.parser=" + parser.name().toLowerCase();
	}

	@Override
	public String toString() {
		return "QParser [parser=" + parser + "]";
	}

	public enum Parser {
		SIMPLE, STRUCTURED, LUCENE, DISMAX
	}

}
