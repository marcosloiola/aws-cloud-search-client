/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

/**
 * Facet
 * 
 * Specifies a field that you want to get facet information for FIELD is the
 * name of the field. The specified field must be facet enabled in the domain
 * configuration. Facet options are specified as a JSON object. If the JSON
 * object is empty, facet.FIELD={}, facet counts are computed for all field
 * values, the facets are sorted by facet count, and the top 10 facets are
 * returned in the results.
 * 
 * You can specify three options in the JSON object:
 * 
 * sort: specifies how you want to sort the facets in the results: bucket or
 * count. Specify bucket to sort alphabetically or numerically by facet value
 * (in ascending order). Specify count to sort by the facet counts computed for
 * each facet value (in descending order). To retrieve facet counts for
 * particular values or ranges of values, use the buckets option instead of
 * sort.
 * 
 * buckets: specifies an array of the facet values or ranges you want to count.
 * Buckets are returned in the order they are specified in the request. To
 * specify a range of values, use a comma (,) to separate the upper and lower
 * bounds and enclose the range using brackets or braces. A square brace, [ or
 * ], indicates that the bound is included in the range, a curly brace, { or },
 * excludes the bound. You can omit the upper or lower bound to specify an
 * open-ended range. When omitting a bound, you must use a curly brace. The sort
 * and size options are not valid if you specify buckets.
 * 
 * size: specifies the maximum number of facets to include in the results. By
 * default, Amazon CloudSearch returns counts for the top 10. The size parameter
 * is only valid when you specify the sort option; it cannot be used in
 * conjunction with buckets.
 * 
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class Facet extends CloudSearchRequestParameter {

	private int size;

	private String field;

	private FacetSort sort;

	private List<String> buckets;

	public Facet(String field) {
		this.field = field;
		this.buckets = new ArrayList<String>();
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public FacetSort getSort() {
		return sort;
	}

	public void setSort(FacetSort sort) {
		this.sort = sort;
	}

	public List<String> getBuckets() {
		return buckets;
	}

	public void setBuckets(List<String> buckets) {
		this.buckets = buckets;
	}

	public void addBuckets(String bucket) {
		this.buckets.add(bucket);
	}

	public void removeBucket(String bucket) {
		this.buckets.remove(bucket);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mlit.aws.cloudsearch.bean.parameters.CloudSearchRequestParameter#
	 * convertToCloudSearch()
	 */
	@Override
	public String convertToCloudSearch() {

		if ((sort != null) || (buckets.size() > 0)) {

			StringBuilder sb = new StringBuilder();

			sb.append("&facet." + field + "={");

			// The sort and size options are not valid if you specify buckets.
			if ((sort != null) && (buckets.size() == 0)) {
				sb.append("sort:").append(new Gson().toJson(sort).toLowerCase());
			}

			// The sort and size options are not valid if you specify buckets.
			// The size parameter is only valid when you specify the sort option
			if ((size > 0) && (buckets.size() == 0) && (sort != null)) {
				sb.append(",size:" + size);
			}

			if (buckets.size() > 0) {
				sb.append("buckets:" + new Gson().toJson(buckets));
			}

			sb.append("}");

			return sb.toString();

		} else {
			return "";
		}

	}

	@Override
	public String toString() {
		return "Facet [size=" + size + ", field=" + field + ", sort=" + sort + ", buckets=" + buckets + "]";
	}

	public enum FacetSort {
		BUCKET, COUNT
	}

}
