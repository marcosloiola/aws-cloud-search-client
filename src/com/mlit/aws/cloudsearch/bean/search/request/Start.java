/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

/**
 * The offset of the first search hit you want to return. You can specify either
 * the start or cursor parameter in a request, they are mutually exclusive. For
 * more information, see Paginating Results.
 * 
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class Start extends CloudSearchRequestParameter {

	private int value;

	public Start(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mlit.aws.cloudsearch.bean.parameters.CloudSearchRequestParameter#
	 * convertToCloudSearch()
	 */
	@Override
	public String convertToCloudSearch() {

		if (value > 0) {
			return "&size=" + value;
		}
		return "";

	}

	@Override
	public String toString() {
		return "Start [value=" + value + "]";
	}

}
