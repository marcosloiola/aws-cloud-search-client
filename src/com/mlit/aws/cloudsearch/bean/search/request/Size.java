/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

/**
 * The maximum number of search hits to return.
 * 
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class Size extends CloudSearchRequestParameter {

	private int value;

	public Size(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mlit.aws.cloudsearch.bean.parameters.CloudSearchRequestParameter#
	 * convertToCloudSearch()
	 */
	@Override
	public String convertToCloudSearch() {
		return "&size=" + value;
	}

	@Override
	public String toString() {
		return "Size [value=" + value + "]";
	}

}
