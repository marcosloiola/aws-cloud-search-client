/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

/**
 * Cursor
 * 
 * Retrieves a cursor value you can use to page through large result sets.To get
 * the first cursor, specify cursor=initial in your initial request. In
 * subsequent requests, specify the cursor value returned in the hits section of
 * the response.
 * 
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class Cursor extends CloudSearchRequestParameter {

	private String value;

	public Cursor(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String convertToCloudSearch() {

		if ((this.value == null) || ("".equals(this.value))) {
			return "&cursor=initial";
		}
		return "&cursor=" + this.value;

	}

	@Override
	public String toString() {
		return "Cursor [value=" + value + "]";
	}

}
