/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

/**
 * Formats JSON output so it's easier to read.
 * 
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class Pretty extends CloudSearchRequestParameter {

	private boolean value = false;

	public Pretty() {

	}

	public Pretty(boolean value) {
		this.value = value;
	}

	public boolean getValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mlit.aws.cloudsearch.bean.parameters.CloudSearchRequestParameter#
	 * convertToCloudSearch()
	 */
	@Override
	public String convertToCloudSearch() {
		return "&pretty=" + value;
	}

	@Override
	public String toString() {
		return "Pretty [value=" + value + "]";
	}

}
