/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;


/**
 * @author Marcos Loiola - marcos.loiola@gmail.com
 */
public class CSSearchRequest {

	private Cursor cursor;

	private Expr expr;

	private Facet facet;

	private Format format;

	private Fq fq;

	private Highlight highlight;

	private Partial partial;

	private Pretty pretty;

	private Q q;

	private QOption qOption;

	private QParser qParser;

	private Return returnFields;

	private Size size;

	private Sort sort;

	private Start start;

	public CSSearchRequest() {
	}

	public CSSearchRequest(Q q) {
		super();

		this.q = q;
	}

	public Cursor getCursor() {
		return cursor;
	}

	public void setCursor(Cursor cursor) {
		this.cursor = cursor;
	}

	public Expr getExpr() {
		return expr;
	}

	public void setExpr(Expr expr) {
		this.expr = expr;
	}

	public Facet getFacet() {
		return facet;
	}

	public void setFacet(Facet facet) {
		this.facet = facet;
	}

	public Format getFormat() {
		return format;
	}

	public void setFormat(Format format) {
		this.format = format;
	}

	public Fq getFq() {
		return fq;
	}

	public void setFq(Fq fq) {
		this.fq = fq;
	}

	public Highlight getHighlight() {
		return highlight;
	}

	public void setHighlight(Highlight highlight) {
		this.highlight = highlight;
	}

	public Partial getPartial() {
		return partial;
	}

	public void setPartial(Partial partial) {
		this.partial = partial;
	}

	public Pretty getPretty() {
		return pretty;
	}

	public void setPretty(Pretty pretty) {
		this.pretty = pretty;
	}

	public Q getQ() {
		return q;
	}

	public void setQ(Q q) {
		this.q = q;
	}

	public QOption getqOption() {
		return qOption;
	}

	public void setqOption(QOption qOption) {
		this.qOption = qOption;
	}

	public QParser getqParser() {
		return qParser;
	}

	public void setqParser(QParser qParser) {
		this.qParser = qParser;
	}

	public Return getReturnFields() {
		return returnFields;
	}

	public void setReturnFields(Return returnFields) {
		this.returnFields = returnFields;
	}

	public Size getSize() {
		return size;
	}

	public void setSize(Size size) {
		this.size = size;
	}

	public Sort getSort() {
		return sort;
	}

	public void setSort(Sort sort) {
		this.sort = sort;
	}

	public Start getStart() {
		return start;
	}

	public void setStart(Start start) {
		this.start = start;
	}

	public String convertToCloudSearch() {

		StringBuilder sb = new StringBuilder();
		sb.append("?");

		if (this.cursor != null) {
			sb.append(this.cursor.convertToCloudSearch());
		}

		if (this.expr != null) {
			sb.append(this.expr.convertToCloudSearch());
		}

		if (this.facet != null) {
			sb.append(this.facet.convertToCloudSearch());
		}

		if (this.fq != null) {
			sb.append(this.fq.convertToCloudSearch());
		}

		if (this.highlight != null) {
			sb.append(this.highlight.convertToCloudSearch());
		}

		if (this.partial != null) {
			sb.append(this.partial.convertToCloudSearch());
		}

		if (this.pretty != null) {
			sb.append(this.pretty.convertToCloudSearch());
		}

		if (this.q != null) {
			sb.append(this.q.convertToCloudSearch());
		}

		if (this.qOption != null) {
			sb.append(this.qOption.convertToCloudSearch());
		}

		if (this.qParser != null) {
			sb.append(this.qParser.convertToCloudSearch());
		}

		if (this.returnFields != null) {
			sb.append(this.returnFields.convertToCloudSearch());
		}

		if (this.size != null) {
			sb.append(this.size.convertToCloudSearch());
		}

		if (this.sort != null) {
			sb.append(this.sort.convertToCloudSearch());
		}

		if (this.start != null) {
			sb.append(this.start.convertToCloudSearch());
		}

		return sb.toString();
	}

	@Override
	public String toString() {
		return "AWSCloudSearchRequest [cursor=" + cursor + ", expr=" + expr + ", facet=" + facet + ", format=" + format + ", fq=" + fq + ", highlight=" + highlight + ", partial=" + partial + ", pretty=" + pretty + ", q=" + q + ", qOption=" + qOption + ", qParser=" + qParser + ", returnFields=" + returnFields + ", size=" + size + ", sort=" + sort + ", start=" + start + "]";
	}

}
