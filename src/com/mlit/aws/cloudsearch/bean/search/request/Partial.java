/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

/**
 * Controls whether partial results are returned if one or more index partitions
 * are unavailable. When your search index is partitioned across multiple search
 * instances, by default Amazon CloudSearch only returns results if every
 * partition can be queried. This means that the failure of a single search
 * instance can result in 5xx (internal server) errors. When you specify
 * partial=true. Amazon CloudSearch returns whatever results are available and
 * includes the percentage of documents searched in the search results
 * (percent-searched). This enables you to more gracefully degrade your users'
 * search experience. For example, rather than displaying no results, you could
 * display the partial results and a message indicating that the results might
 * be incomplete due to a temporary system outage.
 * 
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class Partial extends CloudSearchRequestParameter {

	private boolean value = false;

	public Partial() {

	}

	public Partial(boolean value) {
		this.value = value;
	}

	public boolean getValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mlit.aws.cloudsearch.bean.parameters.CloudSearchRequestParameter#
	 * convertToCloudSearch()
	 */
	@Override
	public String convertToCloudSearch() {
		if (this.value) {
			return "&partial=true";
		}
		return "";
	}

	@Override
	public String toString() {
		return "Partial [value=" + value + "]";
	}

}
