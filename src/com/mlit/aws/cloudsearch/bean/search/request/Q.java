/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

/**
 * The search criteria for the request. How you specify the search criteria
 * depends on the query parser used for the request and the parser options
 * specified in the q.options parameter. By default, the simple query parser is
 * used to process requests. To use the structured, lucene, or dismax query
 * parser, you must also specify the q.parser parameter. For more information
 * about specifying search criteria, see Searching Your Data with Amazon
 * CloudSearch.
 * 
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class Q extends CloudSearchRequestParameter {

	private String value;

	public Q() {
	}

	public Q(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mlit.aws.cloudsearch.bean.parameters.CloudSearchRequestParameter#
	 * convertToCloudSearch()
	 */
	@Override
	public String convertToCloudSearch() {
		return "&q=" + value;
	}

	@Override
	public String toString() {
		return "Q [value=" + value + "]";
	}

}
