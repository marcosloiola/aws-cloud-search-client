/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

import java.util.ArrayList;
import java.util.List;

/**
 * A comma-separated list of fields or custom expressions to use to sort the
 * search results. You must specify the sort direction (asc or desc) for each
 * field. For example, sort=year desc,title asc. You can specify a maximum of 10
 * fields and expressions. To use a field to sort results, it must be sort
 * enabled in the domain configuration. Array type fields cannot be used for
 * sorting. If no sort parameter is specified, results are sorted by their
 * default relevance scores in descending order: sort=_score desc. You can also
 * sort by document ID (sort=_id) and version (sort=_version).
 * 
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class Sort extends CloudSearchRequestParameter {

	private List<String> values;

	public Sort() {
		this.values = new ArrayList<String>();
	}

	public Sort(String value) {
		this.values = new ArrayList<String>();
		this.values.add(value);
	}

	public Sort(List<String> values) {
		this.values = values;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public void addValue(String value) {
		this.values.add(value);
	}

	public void removeValue(String value) {
		this.values.remove(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mlit.aws.cloudsearch.bean.parameters.CloudSearchRequestParameter#
	 * convertToCloudSearch()
	 */
	@Override
	public String convertToCloudSearch() {
		if (values.size() > 0) {

			StringBuilder sb = new StringBuilder();
			sb.append("&sort=");

			for (String value : values) {
				sb.append(value).append(",");
			}
			// Remove the last ,
			sb.deleteCharAt(sb.length() - 1);
			return sb.toString();
		} else {
			return "";
		}
	}

	@Override
	public String toString() {
		return "Sort [values=" + values + "]";
	}

}
