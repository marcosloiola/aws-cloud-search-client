/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

import java.util.ArrayList;
import java.util.List;

/**
 * The field and expression values to include in the response, specified as a
 * comma-separated list. By default, a search response includes all return
 * enabled fields (return=_all_fields). To return only the document IDs for the
 * matching documents, specify return=_no_fields. To retrieve the relevance
 * score calculated for each document, specify return=_score. You specify
 * multiple return fields as a comma separated list. For example,
 * return=title,_score returns just the title and relevance score of each
 * matching document.
 * 
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class Return extends CloudSearchRequestParameter {

	private List<String> values;

	public Return() {
		this.values = new ArrayList<String>();
	}

	public Return(String value) {
		this.values = new ArrayList<String>();
		this.values.add(value);
	}

	public Return(List<String> values) {
		this.values = values;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public void addValue(String value) {
		this.values.add(value);
	}

	public void removeValue(String value) {
		this.values.remove(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mlit.aws.cloudsearch.bean.parameters.CloudSearchRequestParameter#
	 * convertToCloudSearch()
	 */
	@Override
	public String convertToCloudSearch() {

		if (values.size() > 0) {

			StringBuilder sb = new StringBuilder();
			sb.append("&return=");

			for (String value : values) {
				sb.append(value).append(",");
			}
			// Remove the last ,
			sb.deleteCharAt(sb.length() - 1);
			return sb.toString();
		}
		return "";
	}

	@Override
	public String toString() {
		return "Return [values=" + values + "]";
	}

}
