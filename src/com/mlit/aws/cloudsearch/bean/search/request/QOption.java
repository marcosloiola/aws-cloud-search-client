/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

/**
 * 
 * Configure options for the query parser specified in the q.parser parameter.
 * The options are specified as a JSON object, for example:
 * q.options={defaultOperator: 'or', fields: ['title^5','description']}.
 * 
 * The options you can configure vary according to which parser you use:
 * 
 * defaultOperator: The default operator used to combine individual terms in the
 * search string. For example: defaultOperator: 'or'. For the dismax parser, you
 * specify a percentage that represents the percentage of terms in the search
 * string (rounded down) that must match, rather than a default operator. A
 * value of 0% is the equivalent to OR, and a value of 100% is equivalent to
 * AND. The percentage must be specified as a value in the range 0-100 followed
 * by the percent (%) symbol. For example, defaultOperator: 50%. Valid values:
 * and, or, a percentage in the range 0%-100% (dismax). Default: and (simple,
 * structured, lucene) or 100 (dismax). Valid for: simple, structured, lucene,
 * and dismax.
 * 
 * fields: An array of the fields to search when no fields are specified in a
 * search. If no fields are specified in a search and this option is not
 * specified, all text and text-array fields are searched. You can specify a
 * weight for each field to control the relative importance of each field when
 * Amazon CloudSearch calculates relevance scores. To specify a field weight,
 * append a caret (^) symbol and the weight to the field name. For example, to
 * boost the importance of the title field over the description field you could
 * specify: fields: ['title^5','description']. Valid values: The name of any
 * configured field and an optional numeric value greater than zero. Default:
 * All text and text-array fields. Valid for: simple, structured, lucene, and
 * dismax.
 * 
 * operators: An array of the operators or special characters you want to
 * disable for the simple query parser. If you disable the and, or, or not
 * operators, the corresponding operators (+, |, -) have no special meaning and
 * are dropped from the search string. Similarly, disabling prefix disables the
 * wildcard operator (*) and disabling phrase disables the ability to search for
 * phrases by enclosing phrases in double quotes. Disabling precedence disables
 * the ability to control order of precedence using parentheses. Disabling near
 * disables the ability to use the ~ operator to perform a sloppy phrase search.
 * Disabling the fuzzy operator disables the ability to use the ~ operator to
 * perform a fuzzy search. escape disables the ability to use a backslash (\) to
 * escape special characters within the search string. Disabling whitespace is
 * an advanced option that prevents the parser from tokenizing on whitespace,
 * which can be useful for Vietnamese. (It prevents Vietnamese words from being
 * split incorrectly.) For example, you could disable all operators other than
 * the phrase operator to support just simple term and phrase queries:
 * operators:['and', 'not', 'or', 'prefix']. Valid values: and, escape, fuzzy,
 * near, not, or, phrase, precedence, prefix, whitespace. Default: All operators
 * and special characters are enabled. Valid for: simple.
 * 
 * phraseFields: An array of the text or text-array fields you want to use for
 * phrase searches. When the terms in the search string appear in close
 * proximity within a field, the field scores higher. You can specify a weight
 * for each field to boost that score. The phraseSlop option controls how much
 * the matches can deviate from the search string and still be boosted. To
 * specify a field weight, append a caret (^) symbol and the weight to the field
 * name. For example, to boost phrase matches in the title field over the
 * abstract field, you could specify: phraseFields:['title^3', 'abstract'] Valid
 * values: The name of any text or text-array field and an optional numeric
 * value greater than zero. Default: No fields. If you don't specify any fields
 * with phraseFields, proximity scoring is disabled even if phraseSlop is
 * specified. Valid for: dismax.
 * 
 * phraseSlop: An integer value that specifies how much matches can deviate from
 * the search phrase and still be boosted according to the weights specified in
 * the phraseFields option. For example, phraseSlop: 2. You must also specify
 * phraseFields to enable proximity scoring. Valid values: positive integers.
 * Default: 0. Valid for: dismax.
 * 
 * explicitPhraseSlop: An integer value that specifies how much a match can
 * deviate from the search phrase when the phrase is enclosed in double quotes
 * in the search string. (Phrases that exceed this proximity distance are not
 * considered a match.) explicitPhraseSlop: 5. Valid values: positive integers.
 * Default: 0. Valid for: dismax.
 * 
 * tieBreaker: When a term in the search string is found in a document's field,
 * a score is calculated for that field based on how common the word is in that
 * field compared to other documents. If the term occurs in multiple fields
 * within a document, by default only the highest scoring field contributes to
 * the document's overall score. You can specify a tieBreaker value to enable
 * the matches in lower-scoring fields to contribute to the document's score.
 * That way, if two documents have the same max field score for a particular
 * term, the score for the document that has matches in more fields will be
 * higher.
 * 
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class QOption extends CloudSearchRequestParameter {

	private int phraseSlop = 0;

	private int explicitPhraseSlop = 0;

	private double tieBreaker = 0.0;

	private String defaultOperator;

	private List<String> fields;

	private List<String> operators;

	private List<String> phraseFields;

	public QOption() {
		this.fields = new ArrayList<String>();
		this.operators = new ArrayList<String>();
		this.phraseFields = new ArrayList<String>();
	}

	public int getPhraseSlop() {
		return phraseSlop;
	}

	public void setPhraseSlop(int phraseSlop) {
		this.phraseSlop = phraseSlop;
	}

	public int getExplicitPhraseSlop() {
		return explicitPhraseSlop;
	}

	public void setExplicitPhraseSlop(int explicitPhraseSlop) {
		this.explicitPhraseSlop = explicitPhraseSlop;
	}

	public double getTieBreaker() {
		return tieBreaker;
	}

	public void setTieBreaker(double tieBreaker) {
		this.tieBreaker = tieBreaker;
	}

	public String getDefaultOperator() {
		return defaultOperator;
	}

	public void setDefaultOperator(String defaultOperator) {
		this.defaultOperator = defaultOperator;
	}

	public List<String> getFields() {
		return fields;
	}

	public void setFields(List<String> fields) {
		this.fields = fields;
	}

	public void addField(String field) {
		this.fields.add(field);
	}

	public void removeField(String field) {
		this.fields.remove(field);
	}

	public List<String> getOperators() {
		return operators;
	}

	public void setOperators(List<String> operators) {
		this.operators = operators;
	}

	public void addOperator(String operator) {
		this.operators.add(operator);
	}

	public void removeOperator(String operator) {
		this.operators.remove(operator);
	}

	public List<String> getPhraseFields() {
		return phraseFields;
	}

	public void setPhraseFields(List<String> phraseFields) {
		this.phraseFields = phraseFields;
	}

	public void addPhraseField(String phraseField) {
		this.phraseFields.add(phraseField);
	}

	public void removePhraseField(String phraseField) {
		this.phraseFields.remove(phraseField);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mlit.aws.cloudsearch.bean.parameters.CloudSearchRequestParameter#
	 * convertToCloudSearch()
	 */
	@Override
	public String convertToCloudSearch() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("&q.options={");

		if (phraseSlop > 0) {
			sb.append("phraseSlop:").append(this.phraseSlop).append(",");
		}

		if (explicitPhraseSlop > 0) {
			sb.append("explicitPhraseSlop:").append(this.explicitPhraseSlop).append(",");
		}

		if (tieBreaker > 0) {
			sb.append("tieBreaker:").append(this.tieBreaker).append(",");
		}

		if (defaultOperator != null) {
			sb.append("defaultOperator:'").append(this.defaultOperator).append("',");
		}

		if (fields.size() > 0) {
			sb.append("fields:").append(new Gson().toJson(fields)).append(",");
		}

		if (operators.size() > 0) {
			sb.append("operators:").append(new Gson().toJson(operators)).append(",");
		}

		if (phraseFields.size() > 0) {
			sb.append("phraseFields:").append(new Gson().toJson(phraseFields)).append(",");
		}

		if (sb.charAt(sb.length() - 1) == ',') {
			sb.deleteCharAt(sb.length() - 1);
		}

		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toString() {
		return "QOption [phraseSlop=" + phraseSlop + ", explicitPhraseSlop=" + explicitPhraseSlop + ", tieBreaker=" + tieBreaker + ", defaultOperator=" + defaultOperator + ", fields=" + fields + ", operators=" + operators + ", phraseFields=" + phraseFields + "]";
	}

}
