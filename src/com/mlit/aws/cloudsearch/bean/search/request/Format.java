/**
 * 
 */
package com.mlit.aws.cloudsearch.bean.search.request;

/**
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class Format extends CloudSearchRequestParameter {

	private ContentType contentType;

	public Format() {
		this.contentType = ContentType.JSON;
	}

	public Format(ContentType contentType) {
		super();
		this.contentType = contentType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mlit.aws.cloudsearch.bean.request.CloudSearchRequestParameter#
	 * convertToCloudSearch()
	 */
	@Override
	public String convertToCloudSearch() {
		return "&format=" + contentType.name().toLowerCase();
	}

	public ContentType getContentType() {
		return contentType;
	}

	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}

	@Override
	public String toString() {
		return "Format [contentType=" + contentType + "]";
	}

	public enum ContentType {
		HTML, JSON
	}

}
