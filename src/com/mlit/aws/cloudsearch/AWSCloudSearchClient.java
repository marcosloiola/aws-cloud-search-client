package com.mlit.aws.cloudsearch;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import com.mlit.aws.cloudsearch.bean.document.request.CSDocumentBatchRequest;
import com.mlit.aws.cloudsearch.bean.document.response.CSDocumentBatchResponse;
import com.mlit.aws.cloudsearch.bean.search.request.CSSearchRequest;
import com.mlit.aws.cloudsearch.bean.search.response.CSSearchResponse;
import com.mlit.aws.cloudsearch.enums.CloudSearchAPI;
import com.mlit.aws.cloudsearch.util.HttpUtils;

/**
 * @author Marcos Loiola - marcos.loiola@gmail.com
 * 
 */
public class AWSCloudSearchClient {

	private static AWSCloudSearchClient instance = null;
	private String csSearchURL;
	private String csPostDocumentURL;

	public static AWSCloudSearchClient getInstance(String endpoint, CloudSearchAPI api) {
		if (instance == null) {
			instance = new AWSCloudSearchClient(endpoint, api);
		}

		return instance;
	}

	private AWSCloudSearchClient(String endpoint, CloudSearchAPI api) {

		String baseURL = "http://" + endpoint + "/" + api.getApiURL() + "/";
		this.csSearchURL = baseURL + "search";
		this.csPostDocumentURL = baseURL + "documents/batch";

	}

	public CSSearchResponse search(CSSearchRequest csSearchRequest) throws UnsupportedEncodingException {	
		String url = this.csSearchURL + csSearchRequest.convertToCloudSearch();
		System.out.println(url);
		return new HttpUtils().request(url);
	}

	public CSDocumentBatchResponse batch(CSDocumentBatchRequest csDocumentBatchRequest) throws IOException {
		return new HttpUtils().postJsonFile(this.csPostDocumentURL, csDocumentBatchRequest.getFile());
	}

}
